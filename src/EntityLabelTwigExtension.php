<?php

namespace Drupal\entity_label;

use Drupal\Core\Entity\EntityInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Defines and registers Drupal Twig extensions for rendering entity labels.
 */
class EntityLabelTwigExtension extends AbstractExtension {

  /**
   * {@inheritdoc}
   */
  public function getFunctions(): array {
    return [
      new TwigFunction('entity_label', [$this, 'label']),
    ];
  }

  /**
   * Get entity label.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   * @param string $type
   *   The type of label.
   *
   * @return string|null
   *   Returns entity label of the selected type.
   */
  public function label(EntityInterface $entity, string $type = 'singular'): ?string {
    return entity_label_render($entity, $type);
  }

}
