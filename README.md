# Entity Label

Define singular and plural labels with articles on entity bundles. Labels and stored as translatable configurations.
Definite and indefinite articles are separate configurations. Tokens are available to use placeholder replacements.
Check out these examples (labels wrapped in brackets):

- Gerald liked 2 [stories] about quick brown foxes.
- Bobby posted [an idea] about quick brown foxes.
- Charles commented on [a story] about quick brown foxes.
- Jamie loved [the article] about quick brown foxes.

## Usage

1. Download and install the `drupal/entity_label` module. Recommended install method is composer:
   ```
   composer require drupal/entity_label
   ```
2. Edit the desired entity bundle type. For instance, a node content type.
3. Open the "Label settings" section and configure as needed.
4. Render labels using one or more of the provided methods.

## Callable PHP function

```php
t('Bobby posted @label about @title.', [
  '@label' => entity_label_render($entity, 'singular_indefinite_article'),
  '@title' => $entity->label(),
]);
```

## Twig extension

```twig
{% set label = entity_label_render(entity, 'singular_indefinite_article') %}
{% trans %}
  Bobby posted {{ label }} about {{ entity.label() }}.
{% endtrans %}
```

## Replacement tokens

Use the token browser to discover and insert available token replacements. Or use the following examples:

- `[ENTITY_TYPE:label:singular]`
- `[ENTITY_TYPE:label:singular-definite-article]`
- `[ENTITY_TYPE:label:singular-indefinite-article]`
- `[ENTITY_TYPE:label:plural]`
- `[ENTITY_TYPE:label:plural-definite-article]`

> **NOTE:** Replace `ENTITY_TYPE` with the entity bundle type machine name.
